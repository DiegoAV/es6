(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Activity = function () {
    function Activity(id, name, description, location, price, schedule, kind, imgPath, time, favorite) {
        _classCallCheck(this, Activity);

        this.id = id;
        this.name = name;
        this.description = description;
        this.location = location;
        this.price = price;
        this.schedule = schedule;
        this.kind = kind;
        this.imgPath = imgPath;
        this.time = time;
        this.favorite = favorite;
    }

    _createClass(Activity, [{
        key: 'getId',
        value: function getId() {
            return this.id;
        }
    }, {
        key: 'getActivity',
        value: function getActivity() {
            return this.name;
        }
    }, {
        key: 'getDescription',
        value: function getDescription() {
            return this.description;
        }
    }, {
        key: 'getLocation',
        value: function getLocation() {
            return this.location;
        }
    }, {
        key: 'getPrice',
        value: function getPrice() {
            return this.price;
        }
    }, {
        key: 'getSchedule',
        value: function getSchedule() {
            return this.schedule;
        }
    }, {
        key: 'getKind',
        value: function getKind() {
            return this.kind;
        }
    }, {
        key: 'getImgPath',
        value: function getImgPath() {
            return this.imgPath;
        }
    }, {
        key: 'getTime',
        value: function getTime() {
            return this.time;
        }
    }, {
        key: 'getFavorite',
        value: function getFavorite() {
            return this.favorite;
        }
    }, {
        key: 'setFavorite',
        value: function setFavorite(status) {
            this.favorite = status;
        }
    }]);

    return Activity;
}();

window.onload = function () {

    var activitiesResponse = [{
        id: 1,
        name: 'Tour Grutas de García',
        description: 'Ven a conocer uno de los atractivos más destacados de Monterrey a través del Tour Grutas de García, un paseo que te llevará hasta el pueblo de Villa de García para visitar estas famosas grutas con grandes formaciones de estalagmitas.',
        location: { longitude: 25.8546467, latitude: -100.5290047 },
        price: 800,
        schedule: '2018-02-08',
        kind: 'Aventura',
        imgPath: 'https://images.bestday.com/_lib/vimages/Tours/garcia-caves-huasteca-cannyon/fachada_g.jpg',
        time: { hi: '6:00', ht: '17:00' },
        favorite: true
    }, {
        id: 2,
        name: 'Tour Grutas de García',
        description: 'Ven a conocer uno de los atractivos más destacados de Monterrey a través del Tour Grutas de García, un paseo que te llevará hasta el pueblo de Villa de García para visitar estas famosas grutas con grandes formaciones de estalagmitas.',
        location: { longitude: 25.8546467, latitude: -100.5290047 },
        price: 1600,
        schedule: '2018-02-15',
        kind: 'Aventura',
        imgPath: 'https://images.bestday.com/_lib/vimages/Tours/garcia-caves-huasteca-cannyon/fachada_g.jpg',
        time: { hi: '9:00', ht: '17:30' },
        favorite: false
    }, {
        id: 3,
        name: 'Tour Grutas de García',
        description: 'Ven a conocer uno de los atractivos más destacados de Monterrey a través del Tour Grutas de García, un paseo que te llevará hasta el pueblo de Villa de García para visitar estas famosas grutas con grandes formaciones de estalagmitas.',
        location: { longitude: 25.8546467, latitude: -100.5290047 },
        price: 500,
        schedule: '2018-02-02',
        kind: 'Aventura',
        imgPath: 'https://images.bestday.com/_lib/vimages/Tours/garcia-caves-huasteca-cannyon/fachada_g.jpg',
        time: { hi: '10:15', ht: '18:15' },
        favorite: true
    }, {
        id: 4,
        name: 'Tour al Parque Chipinque',
        description: 'El Tour al Parque Chipinque reúne lo mejor de Monterrey y sus alrededores en un solo paquete. Viajarás al majestuoso Parque Chipinque, un área protegida de aproximadamente 1,600 hectáreas en las montañas que rodean a esta metrópoli. Esta zona es ideal para realizar actividades al aire libre, caminatas y observación de flora y fauna. Además, cuenta con varios miradores panorámicos y con un observatorio astronómico.',
        location: { longitude: 25.6086297, latitude: -100.3806988 },
        price: 700,
        schedule: '2018-02-14',
        kind: 'Parques',
        imgPath: 'https://images.bestday.com/_lib/vimages/Tours/tour-chipinque/Gallery/01.jpg',
        time: { hi: '7:00', ht: '13:00' },
        favorite: false
    }, {
        id: 5,
        name: 'Tour al Parque Chipinque',
        description: 'El Tour al Parque Chipinque reúne lo mejor de Monterrey y sus alrededores en un solo paquete. Viajarás al majestuoso Parque Chipinque, un área protegida de aproximadamente 1,600 hectáreas en las montañas que rodean a esta metrópoli. Esta zona es ideal para realizar actividades al aire libre, caminatas y observación de flora y fauna. Además, cuenta con varios miradores panorámicos y con un observatorio astronómico.',
        location: { longitude: 25.6086297, latitude: -100.3806988 },
        price: 1250,
        schedule: '2018-02-15',
        kind: 'Parques',
        imgPath: 'https://images.bestday.com/_lib/vimages/Tours/tour-chipinque/Gallery/01.jpg',
        time: { hi: '6:30', ht: '12:00' },
        favorite: true
    }, {
        id: 6,
        name: 'Masaje Relajante en Monterrey',
        description: 'En tu próxima visita de negocios o vacaciones a la Sultana del Norte, no dudes en reservar el WB Masaje Relajante en Monterrey. En esta actividad visitarás un exclusivo spa de Monterrey para disfrutar un tratamiento profesional con atención personalizada y excelentes productos. Elige entre un masaje de cuerpo completo o una sesión de hidroterapia desintoxicante para tus pies.',
        location: { longitude: 25.6573531, latitude: -100.391792 },
        price: 599,
        schedule: '2018-02-18',
        kind: 'Spa',
        imgPath: 'https://images.bestday.com/_lib/vimages/Tours/wb-masaje-relajante-monterrey/Gallery/02.jpg',
        time: { hi: '10:00', ht: '20:00' },
        favorite: false
    }, {
        id: 7,
        name: 'Cena Oriental en Monterrey',
        description: 'Si deseas una novedosa experiencia culinaria, aprovecha la ocasión y adquiere WB Cena Oriental en Monterrey, un atractivo concepto donde podrás elegir entre dos opciones de menú de tres tiempos acompañado de una copa de vino. En esta actividad probarás delicias orientales en un ambiente relajado y moderno. Aprovecha tus vacaciones en Monterrey y adquiere el WB Cena Oriental en Monterrey, que te llevará a deleitarte con tentadores platillos de la cocina oriental en el barrio de San Jerónimo.',
        location: { longitude: 25.6573853, latitude: -100.391792 },
        price: 800,
        schedule: '2018-02-02',
        kind: 'Vida Nocturna',
        imgPath: 'https://images.bestday.com/_lib/vimages/Tours/wb-cena-oriental-monterrey/Gallery/02.jpg',
        time: { hi: '13:00', ht: '22:00' },
        favorite: false
    }, {
        id: 8,
        name: 'Cena Romántica en las Afueras de Monterrey',
        description: 'Comparte inolvidables momentos con tu pareja reservado la WB Cena Romántica en las Afueras de Monterrey, una experiencia inolvidable aproximadamente a una hora de la capital regiomontana.',
        location: { longitude: 25.6558227, latitude: -100.3741562 },
        price: 3500,
        schedule: '2018-02-14',
        kind: 'Gastronomia',
        imgPath: 'https://images.bestday.com/_lib/vimages/Tours/wb-cena-romantica-fueras-mty/fachada_g.jpg',
        time: { hi: '18:00', ht: '24:00' },
        favorite: true
    }];

    var activitiesArr = [];
    var idActvity = null;
    var arrFilters = [];

    function getNoRandom(max, min, int) {

        return int ? Math.floor(Math.random() * max + min) : Math.random() * max + min;
    }

    function setLocation() {
        if (navigator.geolocation) {
            // navigator.geolocation.watchPosition(showPosition);
            navigator.geolocation.getCurrentPosition(showPosition, showError);
        } else {
            "Geolocation is not supported by this browser.";
        }
    }

    function showPosition(position) {
        var latlon = position.coords.latitude + "," + position.coords.longitude;
        var img_url = "https://maps.googleapis.com/maps/api/staticmap?center=" + latlon + "&zoom=14&size=400x300&sensor=false&key=AIzaSyB9SuddZdPOVmRIVDDhbTjew9aO4-aKmz8";
        document.getElementById("googleMap").innerHTML = "<img async src='" + img_url + "'>";
    }

    function showError(error) {
        switch (error.code) {
            case error.PERMISSION_DENIED:
                x.innerHTML = "User denied the request for Geolocation.";
                break;
            case error.POSITION_UNAVAILABLE:
                x.innerHTML = "Location information is unavailable.";
                break;
            case error.TIMEOUT:
                x.innerHTML = "The request to get user location timed out.";
                break;
            case error.UNKNOWN_ERROR:
                x.innerHTML = "An unknown error occurred.";
                break;
        }
    }

    function createElement(type, nameClass, actId, nodeText) {
        var element = document.createElement(type);

        if (actId) element.setAttribute('id', actId);

        if (nameClass) element.className = nameClass;

        if (nodeText != null) {
            var textNode = document.createTextNode(nodeText);
            element.appendChild(textNode);
        }

        return element;
    }

    function addActivityDom(activity) {
        var div = createElement("DIV", 'row', null);
        div.appendChild(createElement("DIV", 'col-md-2 actname', activity.getId(), activity.getActivity()));
        div.appendChild(createElement("DIV", 'col-md-6', null, activity.getDescription()));

        var img = createElement("div", 'col-md-4', null, null);
        img.innerHTML = "<img async src='" + activity.getImgPath() + "'>";
        div.appendChild(img);

        document.getElementById('divactivity').appendChild(div);
    }

    document.getElementById('loginbtn').onclick = function () {
        // console.log(':p')

        /* let user = "test";
        let pass = "pass";
        let inuser = document.getElementById('user').value;
        let inpass = document.getElementById('passw').value;
        if(inuser === user && inpass === pass){ */
        document.getElementById('login').setAttribute('hidden', true);
        document.getElementById('wrapperlogin').setAttribute('hidden', true);
        document.getElementById('content').removeAttribute('hidden');

        document.body.style.backgroundImage = "none";

        activitiesResponse.forEach(function (activityresp) {
            var activity = new Activity(activityresp.id, activityresp.name, activityresp.description, _defineProperty({ longitude: activityresp.location.latitude }, 'longitude', activityresp.location.longitude), activityresp.price, activityresp.schedule, activityresp.kind, activityresp.imgPath, activityresp.time, activityresp.favorite);
            activitiesArr.push(activity);
            addActivityDom(activity);
        });

        /* for(let i = 1; i<getNoRandom(15,1,true); i++){
            let description = 'Whilst some people consider it overkill to add a ~50 KB framework for simply changing a class, if you are doing any substantial amount of JavaScript work, or anything that might have unusual cross-browser behaviour, it is well worth considering';
            let imgPath = 'http://www.relajacionsinestres.com/wp-content/uploads/2013/03/ocio-y-tiempo-libre-viajes-y-actividades.jpg';
            let activity = new Activity(i,'Mountain cycling',description,{latitude:getNoRandom(200,-200,false),longitude:getNoRandom(200,-200,true)},1500,new Date().toLocaleDateString(),'Run',imgPath, getNoRandom(2,0,true) > 0?true:false);
            activitiesArr.push(activity);
            addActivityDom(activity);
        } */
        // let activity = new Activity('xd');
        console.log(activitiesArr);
        // console.log(activitiesResponse)

        arrFilters = activitiesArr.filter(function (activity) {
            return activity.getId() != 0;
        });
        actInfo();
        // window.location = "./main/main.html";
        // window.location = "C:\Users\Diego\Documents\ES5\recruitmentTest\app\main\main.html"
        /* }else{
            document.getElementById('user').value = "";
            document.getElementById('passw').value = "";
            document.getElementById('errmsg').removeAttribute('hidden')
        } */
    };

    document.getElementById('addActivity').onclick = function () {
        document.getElementById('content').setAttribute('hidden', true);
        document.getElementById('addactcontent').removeAttribute('hidden');
    };

    document.getElementById('addAct').onclick = function () {
        var name = document.getElementById('actName').value;
        var lati = document.getElementById('actLatitude').value;
        var long = document.getElementById('actLongitude').value;
        var price = document.getElementById('actPrice').value;
        var date = document.getElementById('actDate').value;
        var kind = document.getElementById('actKind').value;
        var desc = document.getElementById('actDescription').value;
        var activity = new Activity(activitiesArr.length + 1, name, desc, { latitude: Number(lati), longitude: Number(long) }, Number(price), date, kind, '', false);
        activitiesArr.push(activity);
        console.log(activitiesArr);
    };

    function cleanElement(elementId) {
        var element = document.getElementById(elementId);
        while (element.firstChild) {
            element.removeChild(element.firstChild);
        }
    }

    document.getElementById('return').onclick = function () {
        document.getElementById('content').removeAttribute('hidden');
        document.getElementById('addactcontent').setAttribute('hidden', true);
        cleanElement('divactivity');

        activitiesArr.forEach(function (activity) {
            addActivityDom(activity);
        });

        applyFilters();
    };

    function addActivityDomDetail(activity) {
        var div = createElement("DIV", 'row', null);
        div.appendChild(createElement("DIV", 'col-md-2 actname', null, activity.getActivity()));
        div.appendChild(createElement("DIV", 'col-md-2', null, 'Price: ' + activity.getPrice()));
        div.appendChild(createElement("DIV", 'col-md-2', null, 'Date: ' + activity.getSchedule()));
        div.appendChild(createElement("DIV", 'col-md-2', null, 'Kind: ' + activity.getKind()));
        var check = createElement("DIV", 'col-md-2', null, null);
        check.appendChild(createElement("DIV", 'form-check', null, null));
        var checkbox = createElement("input", 'form-check-input', null, null);
        checkbox.type = 'checkbox';
        checkbox.id = "favorite";
        if (activitiesArr[idActvity].getFavorite()) checkbox.checked = true;
        check.appendChild(checkbox);
        check.appendChild(createElement("label", 'form-check-label', null, 'Favorite'));
        div.appendChild(check);

        document.getElementById('divinfo').appendChild(div);
        div = createElement("DIV", 'row', null);

        var desc = createElement("DIV", 'col-md-6', null, null);
        desc.appendChild(createElement("div", 'row', null, activity.getDescription()));
        var img = createElement("div", 'row', null, null);
        img.innerHTML = "<img async src='" + activity.getImgPath() + "'>";
        desc.appendChild(img);
        div.appendChild(desc);
        var parentMap = createElement("DIV", 'col-md-6', null, null);
        parentMap.setAttribute('id', 'googleMap');
        div.appendChild(parentMap);

        document.getElementById('divinfo').appendChild(div);
        setLocation();
        document.getElementById('favorite').onclick = function () {
            activitiesArr[idActvity].setFavorite(this.checked ? true : false);
            console.log(activitiesArr[idActvity]);
        };
    }

    document.getElementById('returnfrominfo').onclick = function () {
        document.getElementById('content').removeAttribute('hidden');
        document.getElementById('actinfo').setAttribute('hidden', true);
        cleanElement('divinfo');

        applyFilters();
    };

    function actInfo() {
        arrFilters.forEach(function (activity) {
            document.getElementById(activity.getId()).onclick = function () {
                console.log('here');
                document.getElementById('content').setAttribute('hidden', true);
                document.getElementById('actinfo').removeAttribute('hidden');
                idActvity = this.id - 1;
                addActivityDomDetail(activitiesArr[idActvity]);
            };
        });
    }

    var kindFilter = document.getElementById('kindfilter');
    var dateFilter = document.getElementById('dateFilter');

    var minpriceFilter = document.getElementById('minpricefilter');
    var maxpriceFilter = document.getElementById('maxpricefilter');
    var checkOpenFilter = document.getElementById('checkOpenfilter');
    var checkFilter = document.getElementById('checkfilter');

    function applyFilters() {
        // console.log(kindFilter.value,minpriceFilter.value,maxpriceFilter.value,checkFilter.checked)
        arrFilters = activitiesArr.filter(function (activity) {
            return activity.getId() != 0;
        });
        var arrKind = [];
        var arrDate = [];
        var arrMinP = [];
        var arrMaxP = [];
        var arrOpenCheck = [];
        var arrCheck = [];
        if (kindFilter.value != '') {
            arrKind = activitiesArr.filter(function (activity) {
                return activity.getKind().toUpperCase().includes(kindFilter.value.toUpperCase());
            });
            arrFilters = arrFilters.filter(function (activity) {
                return arrKind.indexOf(activity) != -1;
            });
        }
        if (dateFilter.value != '') {
            arrDate = activitiesArr.filter(function (activity) {
                return activity.getSchedule().toString() == dateFilter.value.toString();
            });
            arrFilters = arrFilters.filter(function (activity) {
                return arrDate.indexOf(activity) != -1;
            });
        }
        if (minpriceFilter.value != '') {
            arrMinP = activitiesArr.filter(function (activity) {
                return activity.getPrice() >= minpriceFilter.value;
            });
            arrFilters = arrFilters.filter(function (activity) {
                return arrMinP.indexOf(activity) != -1;
            });
        }
        if (maxpriceFilter.value != '') {
            arrMaxP = activitiesArr.filter(function (activity) {
                return activity.getPrice() <= maxpriceFilter.value;
            });
            arrFilters = arrFilters.filter(function (activity) {
                return arrMaxP.indexOf(activity) != -1;
            });
        }
        if (checkOpenFilter.checked) {
            var regex = new RegExp(':', 'g');
            var time = new Date().getHours() + ':' + new Date().getMinutes();
            arrOpenCheck = activitiesArr.filter(function (activity) {
                return parseInt(time.replace(regex, ''), 10) >= parseInt(activity.getTime().hi.replace(regex, ''), 10) && parseInt(time.replace(regex, ''), 10) <= parseInt(activity.getTime().ht.replace(regex, ''), 10);
            });
            arrFilters = arrFilters.filter(function (activity) {
                return arrOpenCheck.indexOf(activity) != -1;
            });
        }
        if (checkFilter.checked) {
            arrCheck = activitiesArr.filter(function (activity) {
                return activity.getFavorite();
            });
            arrFilters = arrFilters.filter(function (activity) {
                return arrCheck.indexOf(activity) != -1;
            });
        }

        cleanElement('divactivity');

        arrFilters.forEach(function (activity) {
            addActivityDom(activity);
        });

        actInfo();
    }

    dateFilter.onchange = function () {
        applyFilters();
    };

    kindFilter.onkeyup = function () {
        applyFilters();
    };

    minpriceFilter.onkeyup = function () {
        applyFilters();
    };

    maxpriceFilter.onkeyup = function () {
        applyFilters();
    };

    checkOpenFilter.onclick = function () {
        applyFilters();
    };

    checkFilter.onclick = function () {
        applyFilters();
    };
};

},{}]},{},[1]);
